package pe.expropiados.proofmicro.user.adapter.out.persistence;

import lombok.Data;

import javax.persistence.*;
import java.util.UUID;

@Data @Entity
@Table(name = "MT_USER")
public class UserJpaEntity {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name", length = 100, nullable = false)
    private String name;

    @Column(name = "lastname", length = 200, nullable = false)
    private String lastname;

    @Column(name = "username", length = 20, nullable = false)
    private String username;

    @Column(name = "uuid", nullable = false)
    private UUID uuid;
}
