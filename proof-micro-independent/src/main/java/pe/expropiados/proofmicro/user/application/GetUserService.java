package pe.expropiados.proofmicro.user.application;

import lombok.RequiredArgsConstructor;
import pe.expropiados.proofmicro.common.hexagonal.UseCase;
import pe.expropiados.proofmicro.user.application.port.in.GetUserUseCase;
import pe.expropiados.proofmicro.user.application.port.out.GetUserPort;
import pe.expropiados.proofmicro.user.domain.User;

@UseCase
@RequiredArgsConstructor
public class GetUserService implements GetUserUseCase {

    private final GetUserPort getUserPort;

    @Override
    public User getUser(Long id) {
        return getUserPort.getUser(id)
                .orElseThrow(() -> new UserNotFoundException(id));
    }
}
