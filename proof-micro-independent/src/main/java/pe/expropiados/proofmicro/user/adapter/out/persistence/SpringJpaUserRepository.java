package pe.expropiados.proofmicro.user.adapter.out.persistence;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SpringJpaUserRepository extends JpaRepository<UserJpaEntity, Long> {
}
