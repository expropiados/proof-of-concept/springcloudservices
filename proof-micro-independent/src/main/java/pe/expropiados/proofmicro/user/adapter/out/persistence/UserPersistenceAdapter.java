package pe.expropiados.proofmicro.user.adapter.out.persistence;

import lombok.RequiredArgsConstructor;
import pe.expropiados.proofmicro.common.hexagonal.PersistenceAdapter;
import pe.expropiados.proofmicro.user.application.port.in.RegisterUserCommand;
import pe.expropiados.proofmicro.user.application.port.out.GetUserPort;
import pe.expropiados.proofmicro.user.application.port.out.SaveUserPort;
import pe.expropiados.proofmicro.user.domain.User;

import java.util.Optional;
import java.util.UUID;

@PersistenceAdapter
@RequiredArgsConstructor
public class UserPersistenceAdapter implements SaveUserPort, GetUserPort {

    private final SpringJpaUserRepository springDataUserRepository;
    private final UserMapper userMapper;

    @Override
    public Optional<User> getUser(Long id) {
        var userFound = springDataUserRepository.findById(id);
        return userFound.map(userMapper::toUser);
    }

    @Override
    public User saveUser(RegisterUserCommand command, UUID userUUID) {
        var rowToSave = userMapper.toUserJpaEntity(command);
        rowToSave.setUuid(userUUID);
        var rowSaved = springDataUserRepository.save(rowToSave);
        return userMapper.toUser(rowSaved);
    }
}
