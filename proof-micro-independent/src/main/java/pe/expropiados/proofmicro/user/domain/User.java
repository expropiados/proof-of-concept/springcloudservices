package pe.expropiados.proofmicro.user.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Data @Builder
@AllArgsConstructor
public class User {
    private Long id;
    private UUID uuid;
    private String username;
    private String name;
    private String lastname;
}
