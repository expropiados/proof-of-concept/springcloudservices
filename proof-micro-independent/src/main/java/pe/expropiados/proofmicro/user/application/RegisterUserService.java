package pe.expropiados.proofmicro.user.application;

import lombok.RequiredArgsConstructor;
import pe.expropiados.proofmicro.common.hexagonal.UseCase;
import pe.expropiados.proofmicro.user.application.port.in.RegisterUserCommand;
import pe.expropiados.proofmicro.user.application.port.in.RegisterUserUseCase;
import pe.expropiados.proofmicro.user.application.port.out.SaveUserPort;
import pe.expropiados.proofmicro.user.domain.User;

import java.util.UUID;

@UseCase
@RequiredArgsConstructor
public class RegisterUserService implements RegisterUserUseCase {
    private final SaveUserPort saveUserPort;

    @Override
    public User registerUser(RegisterUserCommand command) {
        var userUuid = UUID.randomUUID();
        return saveUserPort.saveUser(command, userUuid);
    }
}
