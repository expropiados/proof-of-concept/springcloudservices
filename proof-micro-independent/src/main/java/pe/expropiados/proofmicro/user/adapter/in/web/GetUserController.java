package pe.expropiados.proofmicro.user.adapter.in.web;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import pe.expropiados.proofmicro.common.hexagonal.WebAdapter;
import pe.expropiados.proofmicro.user.application.port.in.GetUserUseCase;
import pe.expropiados.proofmicro.user.domain.User;

@WebAdapter
@RestController
@RequiredArgsConstructor
public class GetUserController {
    private final GetUserUseCase getUserUseCase;

    @GetMapping("/users/{id}")
    @ApiOperation(value = "Get a user")
    public User getUser(@PathVariable Long id) {
        return getUserUseCase.getUser(id);
    }
}
